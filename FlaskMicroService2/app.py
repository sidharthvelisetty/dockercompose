import imp
from flask import Flask, jsonify, render_template
from consume import consume_data
app=Flask(__name__)

@app.route('/carwebsite', methods= ['GET'])
def index():
    data = consume_data()
    return render_template('index.html', context=data)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5002, debug=True)