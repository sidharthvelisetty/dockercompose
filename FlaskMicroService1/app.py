from flask import Flask, jsonify

app=Flask(__name__)

@app.route('/cars', methods= ['GET'])
def carspec():
    return jsonify({
        'msg': 'This car is a lamborghini',
        'data': 'The mileage is 12',
        'status': 200, 
        'img': 'https://images.unsplash.com/photo-1525609004556-c46c7d6cf023?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=752&q=80'
    })

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5005, debug=True)